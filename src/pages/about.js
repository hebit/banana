import React from "react";
import { useParams } from "react-router-dom";

export default function About() {
  const params = useParams();
  return <h1>Pagina de sobre, com id: {params.id}</h1>;
}
