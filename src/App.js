import React, { useState } from "react";
import Button from "./components/Button";
import { ButtonVV } from "./components/Button/styles";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import Home from "./pages/home";
import About from "./pages/about";

function App() {
  const [contador, setContador] = useState(0);
  return (
    <div className="App">
      O botão foi clickado {contador} vezes{" "}
      <Router>
        <Switch>
          <Route path="/posts/:id" component={About} />
          <Route path="/" component={Home} />
        </Switch>
      </Router>
      {/* <ButtonVV background="red" color="white">
        Botão normal
      </ButtonVV>
      <Button contador={contador} setContador={setContador} /> */}
    </div>
  );
}

export default App;

// JSX = Js + XML <tag />

{
  /* <p>
          Edituou <code>src/App.js</code> and save to reload The Banana
          Blog(+18).
    </p> */
}
