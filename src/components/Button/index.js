import React from "react";
import { ButtonVV } from "./styles";

export default function Button({ setContador, contador, background }) {
  return (
    <ButtonVV
      background={background || "blue"}
      color="black"
      onClick={(e) => {
        console.log(e);
        setContador(contador + 1);
      }}
    >
      Maconha deveria ser legalizada
    </ButtonVV>
  );
}
