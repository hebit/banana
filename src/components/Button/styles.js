import styled from "styled-components";

export const ButtonVV = styled.button`
  background: ${(props) => props.background};
  color: ${(props) => props.color};
`;
